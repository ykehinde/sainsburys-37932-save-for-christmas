/**
 * Author: Yemi Kehinde http://www.kehinde.me
 * 37374-ThankYou
 */

// Create a closure to maintain scope of the '$' and KO
;(function (KO, $) {

	$(function() {

		KO.Config.init();

	}); // END DOC READY


	KO.Config = {
		variableX : '', // please don't keep me - only for example syntax!

		init : function () {
			console.debug('Kickoff is running');
		}
	};

	// Example module
	/*
	KO.MyExampleModule = {
		init : function () {
			KO.MyExampleModule.setupEvents();
		},

		setupEvents : function () {
			//do some more stuff in here
		}
	};
	*/

})(window.KO = window.KO || {}, jQuery);
